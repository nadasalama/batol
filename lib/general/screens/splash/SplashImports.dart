import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/customer/screens/introduction1/IntroductionImports.dart';
import 'package:base_flutter/general/constants/GlobalNotification.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/resources/GeneralRepository.dart';
import 'package:base_flutter/general/utilities/dio_helper/DioImports.dart';
import 'package:base_flutter/general/utilities/utils_functions/UtilsImports.dart';
import 'package:base_flutter/general/widgets/AnimationContainer.dart';
import 'package:base_flutter/general/widgets/MyText.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:base_flutter/res.dart';

import '../../utilities/routers/RouterImports.gr.dart';

part 'Splash.dart';
