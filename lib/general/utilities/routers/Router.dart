part of 'RouterImports.dart';


@AdaptiveAutoRouter(
  routes: <AutoRoute>[
    //general routes
    AdaptiveRoute(page: Splash, initial: true,),
    CustomRoute(page: Login,),
    AdaptiveRoute(page: ForgetPassword),
    AdaptiveRoute(page: ActiveAccount),
    AdaptiveRoute(page: ResetPassword),
    AdaptiveRoute(page: SelectLang),
    AdaptiveRoute(page: Terms),
    AdaptiveRoute(page: About),
    AdaptiveRoute(page: ContactUs),
    CustomRoute(page: SelectUser,transitionsBuilder: TransitionsBuilders.fadeIn,durationInMilliseconds: 1500),
    AdaptiveRoute(page: ConfirmPassword),
    AdaptiveRoute(page: ChangePassword),
    AdaptiveRoute(page: ImageZoom),
    AdaptiveRoute(page: Introduction),
    AdaptiveRoute(page: Introduction2),
    AdaptiveRoute(page: Introduction3),
    AdaptiveRoute(page: MainPage),
    AdaptiveRoute(page: Singers),
    AdaptiveRoute(page: SingersNames),
    AdaptiveRoute(page: SingerDetails),
    AdaptiveRoute(page: DataBooking),
    AdaptiveRoute(page: BackToMain),
    AdaptiveRoute(page: SingersWomen),
    AdaptiveRoute(page: Zafat),
    AdaptiveRoute(page:Tansi2Intro),
    AdaptiveRoute(page:Tansi2),
    AdaptiveRoute(page:Cars),
    AdaptiveRoute(page:CarsDetails),
    AdaptiveRoute(page: Notifications),
    AdaptiveRoute(page: Drawer),
    AdaptiveRoute(page: Orders),
    AdaptiveRoute(page: Offers),
    AdaptiveRoute(page: AcceptOffer),
    AdaptiveRoute(page: Chats),





  ],
)
class $AppRouter {}