// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:base_flutter/customer/screens/accept_offer/AcceptOfferImports.dart'
    as _i34;
import 'package:base_flutter/customer/screens/back_to_main/BackToMainImports.dart'
    as _i24;
import 'package:base_flutter/customer/screens/cars/CarsImports.dart' as _i29;
import 'package:base_flutter/customer/screens/cars_details/CarsDetailsImports.dart'
    as _i30;
import 'package:base_flutter/customer/screens/chats/ChatsImports.dart' as _i35;
import 'package:base_flutter/customer/screens/data_booking/DataBookingImports.dart'
    as _i23;
import 'package:base_flutter/customer/screens/introduction1/IntroductionImports.dart'
    as _i16;
import 'package:base_flutter/customer/screens/introduction2/Introduction2Imports.dart'
    as _i17;
import 'package:base_flutter/customer/screens/introduction3/Introduction3Imports.dart'
    as _i18;
import 'package:base_flutter/customer/screens/main_page/MainPageImports.dart'
    as _i19;
import 'package:base_flutter/customer/screens/notifications/NotificationsImports.dart'
    as _i31;
import 'package:base_flutter/customer/screens/offers/OffersImports.dart'
    as _i33;
import 'package:base_flutter/customer/screens/orders/OrdersImports.dart'
    as _i32;
import 'package:base_flutter/customer/screens/singer_details/SingerDetailsImports.dart'
    as _i22;
import 'package:base_flutter/customer/screens/singers/SingersImports.dart'
    as _i20;
import 'package:base_flutter/customer/screens/singers_names/SingersNamesImports.dart'
    as _i21;
import 'package:base_flutter/customer/screens/singers_women/SingersWomenImports.dart'
    as _i25;
import 'package:base_flutter/customer/screens/tansi2/Tansi2Imports.dart'
    as _i28;
import 'package:base_flutter/customer/screens/tansi2_intro/Tansi2IntroImports.dart'
    as _i27;
import 'package:base_flutter/customer/screens/zafat/ZafatImports.dart' as _i26;
import 'package:base_flutter/general/screens/about/AboutImports.dart' as _i10;
import 'package:base_flutter/general/screens/active_account/ActiveAccountImports.dart'
    as _i6;
import 'package:base_flutter/general/screens/change_password/ChangePasswordImports.dart'
    as _i14;
import 'package:base_flutter/general/screens/confirm_password/ConfirmPasswordImports.dart'
    as _i13;
import 'package:base_flutter/general/screens/contact_us/ContactUsImports.dart'
    as _i11;
import 'package:base_flutter/general/screens/forget_password/ForgetPasswordImports.dart'
    as _i5;
import 'package:base_flutter/general/screens/image_zoom/ImageZoom.dart' as _i15;
import 'package:base_flutter/general/screens/login/LoginImports.dart' as _i4;
import 'package:base_flutter/general/screens/reset_password/ResetPasswordImports.dart'
    as _i7;
import 'package:base_flutter/general/screens/select_lang/SelectLangImports.dart'
    as _i8;
import 'package:base_flutter/general/screens/select_user/SelectUserImports.dart'
    as _i12;
import 'package:base_flutter/general/screens/splash/SplashImports.dart' as _i3;
import 'package:base_flutter/general/screens/terms/TermsImports.dart' as _i9;
import 'package:flutter/cupertino.dart' as _i36;
import 'package:flutter/material.dart' as _i2;

class AppRouter extends _i1.RootStackRouter {
  AppRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    SplashRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<SplashRouteArgs>();
          return _i3.Splash(navigatorKey: args.navigatorKey);
        }),
    LoginRoute.name: (routeData) => _i1.CustomPage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i4.Login();
        },
        opaque: true,
        barrierDismissible: false),
    ForgetPasswordRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i5.ForgetPassword();
        }),
    ActiveAccountRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<ActiveAccountRouteArgs>();
          return _i6.ActiveAccount(userId: args.userId);
        }),
    ResetPasswordRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<ResetPasswordRouteArgs>();
          return _i7.ResetPassword(userId: args.userId);
        }),
    SelectLangRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i8.SelectLang();
        }),
    TermsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i9.Terms();
        }),
    AboutRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i10.About();
        }),
    ContactUsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i11.ContactUs();
        }),
    SelectUserRoute.name: (routeData) => _i1.CustomPage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i12.SelectUser();
        },
        transitionsBuilder: _i1.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 1500,
        opaque: true,
        barrierDismissible: false),
    ConfirmPasswordRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i13.ConfirmPassword();
        }),
    ChangePasswordRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i14.ChangePassword();
        }),
    ImageZoomRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<ImageZoomRouteArgs>();
          return _i15.ImageZoom(images: args.images);
        }),
    IntroductionRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i16.Introduction();
        }),
    Introduction2Route.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i17.Introduction2();
        }),
    Introduction3Route.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i18.Introduction3();
        }),
    MainPageRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i19.MainPage();
        }),
    SingersRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i20.Singers();
        }),
    SingersNamesRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i21.SingersNames();
        }),
    SingerDetailsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i22.SingerDetails();
        }),
    DataBookingRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i23.DataBooking();
        }),
    BackToMainRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i24.BackToMain();
        }),
    SingersWomenRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i25.SingersWomen();
        }),
    ZafatRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i26.Zafat();
        }),
    Tansi2IntroRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i27.Tansi2Intro();
        }),
    Tansi2Route.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i28.Tansi2();
        }),
    CarsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i29.Cars();
        }),
    CarsDetailsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i30.CarsDetails();
        }),
    NotificationsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i31.Notifications();
        }),
    DrawerRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<DrawerRouteArgs>(
              orElse: () => const DrawerRouteArgs());
          return _i2.Drawer(
              key: args.key,
              elevation: args.elevation,
              child: args.child,
              semanticLabel: args.semanticLabel);
        }),
    OrdersRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i32.Orders();
        }),
    OffersRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i33.Offers();
        }),
    AcceptOfferRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i34.AcceptOffer();
        }),
    ChatsRoute.name: (routeData) => _i1.AdaptivePage<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i35.Chats();
        })
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig(SplashRoute.name, path: '/'),
        _i1.RouteConfig(LoginRoute.name, path: '/Login'),
        _i1.RouteConfig(ForgetPasswordRoute.name, path: '/forget-password'),
        _i1.RouteConfig(ActiveAccountRoute.name, path: '/active-account'),
        _i1.RouteConfig(ResetPasswordRoute.name, path: '/reset-password'),
        _i1.RouteConfig(SelectLangRoute.name, path: '/select-lang'),
        _i1.RouteConfig(TermsRoute.name, path: '/Terms'),
        _i1.RouteConfig(AboutRoute.name, path: '/About'),
        _i1.RouteConfig(ContactUsRoute.name, path: '/contact-us'),
        _i1.RouteConfig(SelectUserRoute.name, path: '/select-user'),
        _i1.RouteConfig(ConfirmPasswordRoute.name, path: '/confirm-password'),
        _i1.RouteConfig(ChangePasswordRoute.name, path: '/change-password'),
        _i1.RouteConfig(ImageZoomRoute.name, path: '/image-zoom'),
        _i1.RouteConfig(IntroductionRoute.name, path: '/Introduction'),
        _i1.RouteConfig(Introduction2Route.name, path: '/Introduction2'),
        _i1.RouteConfig(Introduction3Route.name, path: '/Introduction3'),
        _i1.RouteConfig(MainPageRoute.name, path: '/main-page'),
        _i1.RouteConfig(SingersRoute.name, path: '/Singers'),
        _i1.RouteConfig(SingersNamesRoute.name, path: '/singers-names'),
        _i1.RouteConfig(SingerDetailsRoute.name, path: '/singer-details'),
        _i1.RouteConfig(DataBookingRoute.name, path: '/data-booking'),
        _i1.RouteConfig(BackToMainRoute.name, path: '/back-to-main'),
        _i1.RouteConfig(SingersWomenRoute.name, path: '/singers-women'),
        _i1.RouteConfig(ZafatRoute.name, path: '/Zafat'),
        _i1.RouteConfig(Tansi2IntroRoute.name, path: '/tansi2-intro'),
        _i1.RouteConfig(Tansi2Route.name, path: '/Tansi2'),
        _i1.RouteConfig(CarsRoute.name, path: '/Cars'),
        _i1.RouteConfig(CarsDetailsRoute.name, path: '/cars-details'),
        _i1.RouteConfig(NotificationsRoute.name, path: '/Notifications'),
        _i1.RouteConfig(DrawerRoute.name, path: '/Drawer'),
        _i1.RouteConfig(OrdersRoute.name, path: '/Orders'),
        _i1.RouteConfig(OffersRoute.name, path: '/Offers'),
        _i1.RouteConfig(AcceptOfferRoute.name, path: '/accept-offer'),
        _i1.RouteConfig(ChatsRoute.name, path: '/Chats')
      ];
}

class SplashRoute extends _i1.PageRouteInfo<SplashRouteArgs> {
  SplashRoute({required _i36.GlobalKey<_i36.NavigatorState> navigatorKey})
      : super(name,
            path: '/', args: SplashRouteArgs(navigatorKey: navigatorKey));

  static const String name = 'SplashRoute';
}

class SplashRouteArgs {
  const SplashRouteArgs({required this.navigatorKey});

  final _i36.GlobalKey<_i36.NavigatorState> navigatorKey;
}

class LoginRoute extends _i1.PageRouteInfo {
  const LoginRoute() : super(name, path: '/Login');

  static const String name = 'LoginRoute';
}

class ForgetPasswordRoute extends _i1.PageRouteInfo {
  const ForgetPasswordRoute() : super(name, path: '/forget-password');

  static const String name = 'ForgetPasswordRoute';
}

class ActiveAccountRoute extends _i1.PageRouteInfo<ActiveAccountRouteArgs> {
  ActiveAccountRoute({required String userId})
      : super(name,
            path: '/active-account',
            args: ActiveAccountRouteArgs(userId: userId));

  static const String name = 'ActiveAccountRoute';
}

class ActiveAccountRouteArgs {
  const ActiveAccountRouteArgs({required this.userId});

  final String userId;
}

class ResetPasswordRoute extends _i1.PageRouteInfo<ResetPasswordRouteArgs> {
  ResetPasswordRoute({required String userId})
      : super(name,
            path: '/reset-password',
            args: ResetPasswordRouteArgs(userId: userId));

  static const String name = 'ResetPasswordRoute';
}

class ResetPasswordRouteArgs {
  const ResetPasswordRouteArgs({required this.userId});

  final String userId;
}

class SelectLangRoute extends _i1.PageRouteInfo {
  const SelectLangRoute() : super(name, path: '/select-lang');

  static const String name = 'SelectLangRoute';
}

class TermsRoute extends _i1.PageRouteInfo {
  const TermsRoute() : super(name, path: '/Terms');

  static const String name = 'TermsRoute';
}

class AboutRoute extends _i1.PageRouteInfo {
  const AboutRoute() : super(name, path: '/About');

  static const String name = 'AboutRoute';
}

class ContactUsRoute extends _i1.PageRouteInfo {
  const ContactUsRoute() : super(name, path: '/contact-us');

  static const String name = 'ContactUsRoute';
}

class SelectUserRoute extends _i1.PageRouteInfo {
  const SelectUserRoute() : super(name, path: '/select-user');

  static const String name = 'SelectUserRoute';
}

class ConfirmPasswordRoute extends _i1.PageRouteInfo {
  const ConfirmPasswordRoute() : super(name, path: '/confirm-password');

  static const String name = 'ConfirmPasswordRoute';
}

class ChangePasswordRoute extends _i1.PageRouteInfo {
  const ChangePasswordRoute() : super(name, path: '/change-password');

  static const String name = 'ChangePasswordRoute';
}

class ImageZoomRoute extends _i1.PageRouteInfo<ImageZoomRouteArgs> {
  ImageZoomRoute({required List<dynamic> images})
      : super(name,
            path: '/image-zoom', args: ImageZoomRouteArgs(images: images));

  static const String name = 'ImageZoomRoute';
}

class ImageZoomRouteArgs {
  const ImageZoomRouteArgs({required this.images});

  final List<dynamic> images;
}

class IntroductionRoute extends _i1.PageRouteInfo {
  const IntroductionRoute() : super(name, path: '/Introduction');

  static const String name = 'IntroductionRoute';
}

class Introduction2Route extends _i1.PageRouteInfo {
  const Introduction2Route() : super(name, path: '/Introduction2');

  static const String name = 'Introduction2Route';
}

class Introduction3Route extends _i1.PageRouteInfo {
  const Introduction3Route() : super(name, path: '/Introduction3');

  static const String name = 'Introduction3Route';
}

class MainPageRoute extends _i1.PageRouteInfo {
  const MainPageRoute() : super(name, path: '/main-page');

  static const String name = 'MainPageRoute';
}

class SingersRoute extends _i1.PageRouteInfo {
  const SingersRoute() : super(name, path: '/Singers');

  static const String name = 'SingersRoute';
}

class SingersNamesRoute extends _i1.PageRouteInfo {
  const SingersNamesRoute() : super(name, path: '/singers-names');

  static const String name = 'SingersNamesRoute';
}

class SingerDetailsRoute extends _i1.PageRouteInfo {
  const SingerDetailsRoute() : super(name, path: '/singer-details');

  static const String name = 'SingerDetailsRoute';
}

class DataBookingRoute extends _i1.PageRouteInfo {
  const DataBookingRoute() : super(name, path: '/data-booking');

  static const String name = 'DataBookingRoute';
}

class BackToMainRoute extends _i1.PageRouteInfo {
  const BackToMainRoute() : super(name, path: '/back-to-main');

  static const String name = 'BackToMainRoute';
}

class SingersWomenRoute extends _i1.PageRouteInfo {
  const SingersWomenRoute() : super(name, path: '/singers-women');

  static const String name = 'SingersWomenRoute';
}

class ZafatRoute extends _i1.PageRouteInfo {
  const ZafatRoute() : super(name, path: '/Zafat');

  static const String name = 'ZafatRoute';
}

class Tansi2IntroRoute extends _i1.PageRouteInfo {
  const Tansi2IntroRoute() : super(name, path: '/tansi2-intro');

  static const String name = 'Tansi2IntroRoute';
}

class Tansi2Route extends _i1.PageRouteInfo {
  const Tansi2Route() : super(name, path: '/Tansi2');

  static const String name = 'Tansi2Route';
}

class CarsRoute extends _i1.PageRouteInfo {
  const CarsRoute() : super(name, path: '/Cars');

  static const String name = 'CarsRoute';
}

class CarsDetailsRoute extends _i1.PageRouteInfo {
  const CarsDetailsRoute() : super(name, path: '/cars-details');

  static const String name = 'CarsDetailsRoute';
}

class NotificationsRoute extends _i1.PageRouteInfo {
  const NotificationsRoute() : super(name, path: '/Notifications');

  static const String name = 'NotificationsRoute';
}

class DrawerRoute extends _i1.PageRouteInfo<DrawerRouteArgs> {
  DrawerRoute(
      {_i36.Key? key,
      double elevation = 16.0,
      _i36.Widget? child,
      String? semanticLabel})
      : super(name,
            path: '/Drawer',
            args: DrawerRouteArgs(
                key: key,
                elevation: elevation,
                child: child,
                semanticLabel: semanticLabel));

  static const String name = 'DrawerRoute';
}

class DrawerRouteArgs {
  const DrawerRouteArgs(
      {this.key, this.elevation = 16.0, this.child, this.semanticLabel});

  final _i36.Key? key;

  final double elevation;

  final _i36.Widget? child;

  final String? semanticLabel;
}

class OrdersRoute extends _i1.PageRouteInfo {
  const OrdersRoute() : super(name, path: '/Orders');

  static const String name = 'OrdersRoute';
}

class OffersRoute extends _i1.PageRouteInfo {
  const OffersRoute() : super(name, path: '/Offers');

  static const String name = 'OffersRoute';
}

class AcceptOfferRoute extends _i1.PageRouteInfo {
  const AcceptOfferRoute() : super(name, path: '/accept-offer');

  static const String name = 'AcceptOfferRoute';
}

class ChatsRoute extends _i1.PageRouteInfo {
  const ChatsRoute() : super(name, path: '/Chats');

  static const String name = 'ChatsRoute';
}
