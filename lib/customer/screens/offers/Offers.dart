part of 'OffersImports.dart';

class Offers extends StatefulWidget {
  @override
  _OffersState createState() => _OffersState();
}

class _OffersState extends State<Offers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "العروض",
      ),
      backgroundColor: MyColors.secondary,
      body:
      Container( margin:  const EdgeInsets.symmetric(vertical: 12,horizontal: 10)
        ,child: ListView.builder(
          itemCount: 8,
          itemBuilder: (context,index){
            return BuildItemOffers();
          },
        ),
      ),
    );
  }
}
