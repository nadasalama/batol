part of 'WidgetsOffersImports.dart';

class BuildItemOffers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
      height: 150,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          border: Border.all(color: MyColors.primary),
          borderRadius: BorderRadius.circular(10)),
      child: Column( mainAxisAlignment: MainAxisAlignment.spaceBetween
       , children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CachedImage(
                url:
                    'https://arabicguitarclick.com/wp-content/uploads/2019/02/1-1024x620.jpg',
                height: 60,
                width: 70,
                borderRadius: BorderRadius.circular(20),
              ),
              Column(
                children: [
                  MyText(
                      title: "اسم مقدم الخدمه",
                      color: MyColors.white,
                      size: 10),
                  Image.asset(Res.star,height: 20,width: 20,)
                ],
              ),
              Container(
                height: 40,
                width: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: MyColors.primary,
                ),
                child:
                InkWell( onTap: ()=>AutoRouter.of(context).push(AcceptOfferRoute())
                 , child: Center(
                      child:
                          MyText(title: "قبول", color: MyColors.black, size: 9)),
                ),
              )
            ],
          )
          //SizedBox(height: 15,),
        ,  Row( mainAxisAlignment: MainAxisAlignment.spaceBetween
            ,children: [
              MyText(title: "تكلفه الخدمه", color: MyColors.white, size: 10,alien: TextAlign.end,),
              MyText(title: "2000ر.س", color: MyColors.primary, size: 10,alien: TextAlign.end,)
            ],
          )
        ],
      ),
    );
  }
}
