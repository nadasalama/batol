part of 'widgetsTansi2Imports.dart';

class BuildTapBarTansi2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container( margin:  const EdgeInsets.symmetric(vertical: 15,horizontal: 5)
     , child: TabBar(
        tabs: [
          Tab(
              child: InkWell(
            // onTap: ()=>AutoRouter.of(context).push(CurrentRoute()),
            child: Container(
              width: 150,
              height: 40,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: MyColors.primary),
              child:
              Center(
                child: MyText(
                  title: "تنسيق الاماكن ",
                  color: MyColors.black,
                  size: 11,
                  alien: TextAlign.center,
                ),
              ),
            ),
          )),
          Tab(
            child: InkWell(
              //  onTap: ()=>AutoRouter.of(context).push(FinishedRoute()),
              child: Container(
                width: 150,
                height: 40,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: MyColors.primary),
                child:
                Center(
                  child: MyText(
                    title: "تنسيق هدايا",
                    color: MyColors.black,
                    size: 11,
                    alien: TextAlign.center,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
