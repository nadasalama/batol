part of 'widgetsTansi2Imports.dart';

class BuildItemTansi2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      decoration: BoxDecoration(
          border: Border.all(color: MyColors.primary.withOpacity(0.5)),
          borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          CachedImage(
            url:
                "https://hasnae.com/wp-content/uploads/2014/11/beautiful-places-Hasnae.com_.9.jpg",
            height: 80,
            width: 80,
            borderRadius: BorderRadius.circular(10),
          ),
          Container(
            margin:  const EdgeInsets.symmetric(vertical: 10,horizontal: 10)
          ,  child: Column(
              children: [
                MyText(title: "اسم مقدم الخدمه", color: MyColors.white.withOpacity(0.5), size: 11),
                Image.asset(Res.star,color: MyColors.primary,)
              ],
            ),
          )
        ],
      ),
    );
  }
}
