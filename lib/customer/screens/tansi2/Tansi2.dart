part of 'Tansi2Imports.dart';

class Tansi2 extends StatefulWidget {
  @override
  _Tansi2State createState() => _Tansi2State();
}

class _Tansi2State extends State<Tansi2> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold( bottomNavigationBar:
      InkWell( onTap: ()=>AutoRouter.of(context).push(DataBookingRoute())
        , child: Container( height: 40
          , margin: const EdgeInsets.all(10)
          , decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),color: MyColors.primary),

          child: MyText(
            title: "طلب للكل",
            color: MyColors.white,
            size: 13,alien: TextAlign.center,
          ),
        ),
      ),
        appBar: DefaultAppBar(
          title: "تنسيق",
        ),
        backgroundColor: MyColors.secondary,
        body: Column(
          children: [
            BuildTapBarTansi2(),
            Flexible(
                child: TabBarView(
              children: [

                Places(),
                Gifts(),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
