part  of'OrderAllImports.dart';
class OrderAll extends StatefulWidget {
  @override
  _OrderAllState createState() => _OrderAllState();
}

class _OrderAllState extends State<OrderAll> {
  OrderAllData orderAllData=OrderAllData();

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      bottomNavigationBar:
      InkWell( onTap: ()=>AutoRouter.of(context).push(BackToMainRoute())
        , child: Container( height: 40
          , margin: const EdgeInsets.all(10)
          , decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),color: MyColors.primary),

          child: MyText(
            title: "احجز",
            color: MyColors.black,
            size: 13,alien: TextAlign.center,
          ),
        ),
      ),
      appBar: DefaultAppBar(
        title: "تنسيق",
      ),
      backgroundColor: MyColors.secondary,
      body:
      ListView(
        padding: const EdgeInsets.symmetric(vertical: 10)
        ,  children: [
        BuildContainerBooking(
          text: "التاريخ",
          label: "رجاء ادخل التاريخ",
        ),
        BuildContainerBooking(
          text: "رقم جوال للتواصل",
          label: "رجاء ادخل الرقم",
        ),
        BuildContainerBooking(
          text: "الوقت ",
          label: "رجاء ادخل الوقت",
        ),
        MyText(title: "المكان", color: MyColors.white, size: 12),


        Container(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
          margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
          child: IconTextFiled(
              label: "رجاء ادخل المكان",
              controller: orderAllData.desc,
            validate: (value) => value!.validateEmpty(context),
          ),
        )

        ,  Container(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
          margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
          child: RichTextFiled(
            label: "الوصف",
            controller: orderAllData.desc,
            max: 3,
            fillColor: MyColors.secondary,
            validate: (value) => value!.validateEmpty(context),
          ),
        )
      ],
      ),
    );
  }
}
