part of 'NotificationsImports.dart';
class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(title: "الاشعارات"),
      backgroundColor: MyColors.secondary,
        body: ListView.builder(
          itemCount: 10,
          itemBuilder: (context,index){
            return BuildItemNotifications();
          },
        ),
    );
  }
}
