import 'package:base_flutter/customer/screens/notifications/widgets/WidgetsImports.dart';
import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
part 'NotificationsData.dart';
part 'Notifications.dart';