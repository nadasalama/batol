part of 'WidgetsImports.dart';

class BuildItemNotifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 7),
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      height: 70,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          border: Border.all(color: MyColors.black.withOpacity(0.3))
      ),
      child: Row(
        children: [
          CachedImage(
            url:
                "https://arabicguitarclick.com/wp-content/uploads/2019/02/1-1024x620.jpg",
            height: 50,
            width: 60,
            borderRadius: BorderRadius.circular(45),
          ),
          Column(
            children: [
              MyText(
                  title: "يوجد عروض برجاء مراجعه الطلب",
                  color: MyColors.white,
                  size: 9),
              MyText(
                  title: "منذ 12دقيقه",
                  color: MyColors.primary,
                  size: 9),
            ],
          )
        ],
      ),
    );
  }
}
