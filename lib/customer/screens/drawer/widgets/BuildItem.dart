part of 'WidgetsImports.dart';

class BuildItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container( color:  MyColors.secondary,height: 60
     , child: Row(
        children: [
          (CircleAvatar(child: Image.asset(Res.icon),)),
          Column(
            children: [
              MyText(
                color: MyColors.white,
                title: "محمد علي",
                size: 10,
              ),
              MyText(
                  title: " mohamed@gmail.com ",
                  color: MyColors.primary,
                  size: 9)
            ],
          )
        ],
      ),
    );
  }
}
