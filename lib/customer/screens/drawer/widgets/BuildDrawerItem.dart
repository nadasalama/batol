part of 'WidgetsImports.dart';

class BuildDrawerItem extends StatelessWidget {
  final String text;
  final IconData icon;

  const BuildDrawerItem({required this.text, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: MediaQuery.of(context).size.width,
      //margin: const EdgeInsets.symmetric(vertical: 25),
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      color: MyColors.secondary,
      child: Row(
        children: [
          Icon(icon
            ,
            color: MyColors.white,
          ),SizedBox(width: 10,)
         , MyText(title: text, color: MyColors.white, size: 10)
        ],
      ),
    );
  }
}
