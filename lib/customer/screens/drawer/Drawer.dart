part of 'DrawerImports.dart';

class HomeDrawer extends StatefulWidget {
  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 0,
      child: Container(
        child: ListView(children: [
          BuildItem(),
          InkWell(
              onTap: () {},
              child: BuildDrawerItem(text: " الرئيسية", icon: Icons.home)),
          InkWell(
              onTap: ()=>AutoRouter.of(context).push( OrdersRoute()),
              child:
                  BuildDrawerItem(text: " الطلبات", icon: Icons.border_color)),
          InkWell(
              onTap: () =>AutoRouter.of(context).push(ChatsRoute()),
              child: BuildDrawerItem(text: " المحادثات ", icon: Icons.chat)),
          InkWell(
              onTap: () {},
              child: BuildDrawerItem(
                  text: " تسجيل كمقدم خدمه", icon: Icons.person)),
          InkWell(
              onTap: () {},
              child:
                  BuildDrawerItem(text: " الملف الشخصى ", icon: Icons.person)),
          InkWell(
            onTap: () {},
            child: BuildDrawerItem(
                text: " الشروط والاحكام",
                icon: Icons.indeterminate_check_box_sharp),
          ),
          InkWell(
              onTap: () =>AutoRouter.of(context).push(AboutRoute()),
              child: BuildDrawerItem(
                  text: " من نحن", icon: Icons.app_blocking_outlined)),
          InkWell(
              onTap: () =>AutoRouter.of(context).push(ContactUsRoute()),
              child: BuildDrawerItem(text: " اتصل بنا ", icon: Icons.call)),
          InkWell(
              onTap: () =>AutoRouter.of(context).push(SelectLangRoute()),
              child: BuildDrawerItem(text: " اللغة ", icon: Icons.language)),
          InkWell(
              onTap: () {},
              child: BuildDrawerItem(
                  text: " مشاركه التطبيق  ", icon: Icons.share)),
          InkWell(
              onTap: () {},
              child: BuildDrawerItem(text: " اتصل بنا ", icon: Icons.call)),
          InkWell(
              onTap: () {},
              child:
                  BuildDrawerItem(text: " تسجيل الخروج ", icon: Icons.logout))
        ]),
      ),
    );
  }
}
