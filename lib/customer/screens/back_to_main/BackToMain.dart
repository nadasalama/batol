part of 'BackToMainImports.dart';

class BackToMain extends StatefulWidget {
  BackToMainData backToMainData = BackToMainData();

  @override
  _BackToMainState createState() => _BackToMainState();
}

class _BackToMainState extends State<BackToMain> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      bottomNavigationBar: InkWell(
          onTap: () => AutoRouter.of(context).push(MainPageRoute()),
          child: Container(
            height: 40,
            margin: const EdgeInsets.all(20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: MyColors.primary),
            child: MyText(
              title: "العودة للرئيسيه",
              color: MyColors.black,
              size: 13,
              alien: TextAlign.center,
            ),
          )),
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 30, horizontal: 5),
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
        child: Center(
          child: Column(
            //  mainAxisAlignment: MainAxisAlignment.spaceBetween
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(Res.success),
              MyText(
                  title: "تم ارسال طلبك بنجاح",
                  color: MyColors.white,
                  size: 17),
              MyText(
                  title:
                      "هذا النص هو مثال للنص الذى يمكن توليده ف مثل هذة المساحه"
                          "هذا النص هو مثال للنص الذى يمكن توليده ف مثل هذة المساحه",
                  color:  MyColors.white.withOpacity(0.5),
                  size: 10)
            ],
          ),
        ),
      ),
    );
  }
}
