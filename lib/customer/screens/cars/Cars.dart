part of 'CarsImports.dart';

class Cars extends StatefulWidget {
  @override
  _CarsState createState() => _CarsState();
}

class _CarsState extends State<Cars> {
  CarsData carsData = CarsData();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: DefaultAppBar(
          title: "سيارات",
        ),
        backgroundColor: MyColors.secondary,
        body: Column(
          children: [
            IconTextFiled(
              label: " بحث",
              controller: carsData.saerch,
              validate: (value) => value!.validateEmpty(context),
              suffixIcon: Icon(Icons.search_rounded),
              margin: EdgeInsets.symmetric(vertical:10,horizontal: 10),
              filledColor: MyColors.secondary,
            ),
            Flexible(
              child: ListView.builder(
                itemCount: 6,
                itemBuilder: (context, index) {
                  return BuildItemCars(
                      image:
                      "https://www.arabsauto.com/wp-content/uploads/2020/12/Lamborghini-SC20-4-780x470.jpg",
                      title: "اسم السياره",
                      onTap: () =>
                          AutoRouter.of(context).push(CarsDetailsRoute()),
                      price: "1000ر.س");
                },
              ),
            ),
          ],
        ));
  }
}
