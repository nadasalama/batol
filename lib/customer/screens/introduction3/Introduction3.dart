part of 'Introduction3Imports.dart';

class Introduction3 extends StatefulWidget {
  @override
  _Introduction3State createState() => _Introduction3State();
}

class _Introduction3State extends State<Introduction3> {


  @override
  void initState() {
    Timer(
        Duration(seconds: 3),
            (){
          AutoRouter.of(context).push(LoginRoute());
        }
    );
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
     // onTap: () =>AutoRouter.of(context).push(LoginRoute()),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: BuildIntroduction(
          image: Res.intro3,
        ),
      ),
    );
  }
}
