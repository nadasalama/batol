import 'package:base_flutter/general/constants/MyColors.dart';
import 'package:base_flutter/general/widgets/DefaultAppBar.dart';
import 'package:flutter/material.dart';
 import 'package:base_flutter/customer/screens/orders/widgets/OrdersWidgetsImports.dart';
 import 'package:base_flutter/customer/screens/orders/tabs/active_orders/ActiveOrdersImports.dart';
 import 'package:base_flutter/customer/screens/orders/tabs/finished_orders/FinishedOrdersImports.dart';
part 'Orders.dart';
part 'OrdersData.dart';