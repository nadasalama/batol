part of 'OrdersWidgetsImports.dart';

class BuildItemList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      InkWell( onTap: ()=>AutoRouter.of(context).push(OffersRoute())
      ,  child: Container(
        margin: const EdgeInsets.symmetric(vertical:10 , horizontal: 5),
        padding: const EdgeInsets.symmetric(vertical:5, horizontal:7),
        height: 90,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            border: Border.all(color: MyColors.primary),
            borderRadius: BorderRadius.circular(10)),
        child: Row( mainAxisAlignment:  MainAxisAlignment.spaceBetween
         , children: [
            CachedImage(
              url:
                  "https://arabicguitarclick.com/wp-content/uploads/2019/02/1-1024x620.jpg",
              height: 40,
              width: 60,
              borderRadius: BorderRadius.circular(20),
            ),
            Column(
              children: [
                MyText(title: "اسم مقدم الخدمه", color: MyColors.white, size: 10),
                MyText(title: "ارقم الطلب 33", color: MyColors.white, size: 9)
              ],
            ),
            MyText(title: "3.5م", color: MyColors.primary, size: 9)
          ],
        ),
    ),
      );
  }
}
