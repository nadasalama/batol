part of 'OrdersWidgetsImports.dart';

class BuildTapBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TabBar(
        tabs: [
          Tab(
            child: InkWell(
              // onTap: ()=>AutoRouter.of(context).push(CurrentRoute()),
              child: Container(
                width: 120,
                height: 40,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: MyColors.primary),
                child: MyText(
                  title: "الطلبات النشطه ",
                  color: MyColors.white,
                  size: 10,
                  alien: TextAlign.center,
                ),
              ),
            ),
          ),
          Tab(
            child: InkWell(
              // onTap: ()=>AutoRouter.of(context).push(CurrentRoute()),
              child: Container(
                width: 120,
                height: 40,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: MyColors.primary),
                child: MyText(
                  title: "الطلبات المنتهيه ",
                  color: MyColors.white,
                  size: 10,
                  alien: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
