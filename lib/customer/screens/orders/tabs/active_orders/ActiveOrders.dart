part of 'ActiveOrdersImports.dart';

class ActiveOrders extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.secondary,
        body: Container(
          margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
          child: ListView.builder(
            itemCount: 8,
            itemBuilder: (context, index) {
              return BuildItemList();
            },
          ),
        ));
  }
}
