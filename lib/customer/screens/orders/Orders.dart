part of 'OrdersImports.dart';

class Orders extends StatefulWidget {
  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: MyColors.secondary,
        appBar: DefaultAppBar(
          title: "الطلبات",
        ),
        body: Column(
          children: [
            BuildTapBar(),
            Flexible(
                child: TabBarView(
              children: [ActiveOrders(), FinishedOrders()],
            ))
          ],
        ),
      ),
    );
  }
}
