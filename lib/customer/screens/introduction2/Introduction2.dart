part of 'Introduction2Imports.dart';

class Introduction2 extends StatefulWidget {
  @override
  _Introduction2State createState() => _Introduction2State();
}

class _Introduction2State extends State<Introduction2> {
  Introduction2Data introduction2data = Introduction2Data();
  @override
  void initState() {
    Timer(
        Duration(seconds: 3),
            (){
          AutoRouter.of(context).push(Introduction3Route());
        }
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: BuildIntroduction(
        image: Res.intro2,
       // onTap: () => AutoRouter.of(context).push(Introduction3Route()),
      ),
    );
  }
}
