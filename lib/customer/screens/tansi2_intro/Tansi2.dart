part of 'Tansi2IntroImports.dart';

class Tansi2Intro extends StatefulWidget {
  @override
  _Tansi2IntroState createState() => _Tansi2IntroState();
}

class _Tansi2IntroState extends State<Tansi2Intro> {
  @override
  void initState() {
    Timer(
      Duration(seconds: 3),
        (){
        AutoRouter.of(context).push(Tansi2Route());
        }
    );
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CachedImage(
        url:
            "https://png.pngtree.com/png-clipart/20200401/original/pngtree-wild-abstract-rock-guitarist-decoration-png-image_5338652.jpg",
        fit: BoxFit.fill,
      ),
    );
  }
}
