part of 'IntroductionImports.dart';

class Introduction extends StatefulWidget {
  @override
  _IntroductionState createState() => _IntroductionState();
}

class _IntroductionState extends State<Introduction> {

  @override
  void initState() {
    Timer(
        Duration(seconds: 3),
            (){
          AutoRouter.of(context).push(Introduction2Route());
        }
    );
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: BuildIntroduction(
        image: Res.intro1,
      //  onTap: () => AutoRouter.of(context).push(Introduction2Route()),
      ),
    );
  }
}
