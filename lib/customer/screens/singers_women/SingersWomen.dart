part  of'SingersWomenImports.dart';
class SingersWomen extends StatefulWidget {
  SingersWomenData singersWomenData=SingersWomenData();
  @override
  _SingersWomenState createState() => _SingersWomenState();
}

class _SingersWomenState extends State<SingersWomen> {
  @override
  Widget build(BuildContext context) {
    return   Scaffold(
        backgroundColor: MyColors.secondary
        ,appBar: DefaultAppBar(
          title: "المطربات",
        ),
        body:
        ListView.builder(
          itemCount: 6,
          itemBuilder: (context, index) {
            return BuildNamesList( onTap:(){
              return AutoRouter.of(context).push(SingerDetailsRoute());},
              title: "اسم المطرب",
              image:
              "https://scenenoise.com/Content/Articles/Small_image/636829017164271376_Capture4433.JPG",
              price: "1000ر.س",
            );
          },

        )

    );
  }
}
