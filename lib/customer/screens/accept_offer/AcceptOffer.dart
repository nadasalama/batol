part of 'AcceptOfferImports.dart';

class AcceptOffer extends StatefulWidget {
  @override
  _AcceptOfferState createState() => _AcceptOfferState();
}

class _AcceptOfferState extends State<AcceptOffer> {


  @override
  void initState() {
    Timer(
        Duration(seconds: 3),
            (){
          AutoRouter.of(context).push(ChatsRoute());
        }
    );
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 50, vertical: 50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CachedImage(
                    url: "https://lh3.googleusercontent."
                        "com/proxy/ZJFLyzXBnOwznHBEagzbU0zUrsHBq3Gkdg_rIAm8z_xV_6V9woV9ZSZBNr"
                        "PdNH0ob9aD15sleJ3mhOSZnooVaw_pAmAJsACZ_ug",
                    height: 200,
                    width: 150,
                  ),
                  MyText(
                      title: "تم قبول العرض", color: MyColors.white, size: 15),
                ],
              ),
            ),
            MyText(
                title: "برجاء الانتظار سيتم تحويلك الى المحادثات الان",
                color: MyColors.white,
                size: 12)
          ],
        ),
      ),
    );
  }
}
