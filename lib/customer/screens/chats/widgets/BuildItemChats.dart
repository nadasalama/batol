part of 'WidgetsChatsImports.dart';

class BuildItemChats extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
      padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 5),
      height: 60,
      width: MediaQuery.of(context).size.width,

      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween
       , children: [
          Row(
            children: [
              CachedImage(
                url:
                    "https://png.pngtree.com/element_our/png_detail/20180918/guitar-player-png_102190.jpg",
                height: 60,
                width: 60,
                borderRadius: BorderRadius.circular(10),
              ),
              Column(
                children: [
                  MyText(title: "محمد رمضان", color: MyColors.white, size: 11),
                  MyText(
                      title: "اهلا وسهلا بحضرتك",
                      color: MyColors.white.withOpacity(0.5),
                      size: 9)
                ],
              ),
            ],
          ),

        MyText(title: "9.5م", color: MyColors.primary, size: 9)
        ],
      ),
    );
  }
}
