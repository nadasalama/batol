part of 'ChatsImports.dart';

class Chats extends StatefulWidget {
  @override
  _ChatsState createState() => _ChatsState();
}

class _ChatsState extends State<Chats> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "المحادثات",
      ), backgroundColor: MyColors.secondary
      ,body:  ListView.builder(
      itemCount: 10,
      itemBuilder: (context,index){
        return BuildItemChats();
      },
    ),
    );
  }
}
