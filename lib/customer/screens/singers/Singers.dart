part of 'SingersImports.dart';

class Singers extends StatefulWidget {
  @override
  _SingersState createState() => _SingersState();
}

class _SingersState extends State<Singers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.secondary,
      appBar: DefaultAppBar(
        title: "المطربين",
      ),
      body: ListView.builder(
        itemCount: 6,
        itemBuilder: (context, index) {
          return BuildListSinger(
            title: "اسم القسم",
            image:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWqAY_IGJZ6Z0aO49LlJGa4Q61fONtbcXdSw&usqp=CAU',
            onTap: ()=>AutoRouter.of(context).push(SingersNamesRoute()),
          );
        },
      ),
    );
  }
}
