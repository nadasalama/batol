part of 'WidgetsSingersImports.dart';

class BuildListSinger extends StatelessWidget {
  final String image;
  final Function()? onTap;
  final String title;

  const BuildListSinger({Key? key, required this.image, this.onTap, required this.title}) ;

  @override
  Widget build(BuildContext context) {
    return
      InkWell( onTap: onTap,
        child: Container(
          margin:  const EdgeInsets.symmetric(vertical: 10,horizontal: 10)
         , child: Column(
        children: [
          CachedImage(
            url: image,
            height: 170,
            width: 170,
            borderRadius: BorderRadius.circular(10),
            borderColor: MyColors.primary.withOpacity(0.5)
           // colorFilter: ColorFilter.mode(MyColors.white, BlendMode.darken),
          ),
          MyText(title: title, color: MyColors.white, size: 12)
        ],
    )),
      );
  }
}
