part of 'WidgetsImports.dart';

class BuildItemZafat extends StatelessWidget {
  final String image;
  final String text1;
  final String text2;

  const BuildItemZafat({required this.image,required this.text1,required this.text2});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      child: Column(
        children: [
          CachedImage(
            url: image,
            height: 200,
            width: MediaQuery.of(context).size.width,
            borderRadius: BorderRadius.circular(10),
            borderColor: MyColors.primary.withOpacity(0.5),
          ),
          Container( margin:  const EdgeInsets.symmetric(vertical: 10,horizontal: 5)
           , child: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween
            ,  children: [
                MyText(title: text1, color: MyColors.white, size: 10),
                MyText(title: text2, color: MyColors.white.withOpacity(0.5), size: 10)
              ],
            ),
          )
        ],
      ),
    );
  }
}
