part of 'ZafatImports.dart';

class Zafat extends StatefulWidget {
  @override
  _ZafatState createState() => _ZafatState();
}

class _ZafatState extends State<Zafat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "زفات",
      ),
      backgroundColor: MyColors.secondary,
      body:
      Container(
        padding:  const EdgeInsets.symmetric(vertical: 5,horizontal: 10)
       , child: ListView.builder(
          itemCount: 6,
          itemBuilder: (context, index) {
            return BuildItemZafat(
              image: "https://img.lovepik.com/photo/50144/1044.jpg_wh860.jpg",
              text1: "زفات باسم",
              text2: "1000ر.س",
            );
          },
        ),
      ),
    );
  }
}
