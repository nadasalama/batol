part of 'CarsDetailsImports.dart';

class CarsDetails extends StatefulWidget {
  @override
  _CarsDetailsState createState() => _CarsDetailsState();
}

class _CarsDetailsState extends State<CarsDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(
        title: "سياراتك",
      ),
      backgroundColor: MyColors.secondary,
      bottomNavigationBar: InkWell(
        onTap: () => AutoRouter.of(context).push(DataBookingRoute()),
        child: Container(
          height: 40,
          margin: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30), color: MyColors.primary),
          child: MyText(
            title: "احجز",
            color: MyColors.black,
            size: 13,
            alien: TextAlign.center,
          ),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
        child: ListView(
          children: [
            BuildSwiper(
              image:
                  "https://cdn.al-ain.com/images/2020/5/06/140-033238-best-looking-cars-trucks-suvs-2020_700x400.jpg",
            ),
            MyText(title: "الوصف", color: MyColors.primary, size: 13),
            MyText(
                title: "هذا النص هو مثال للنص الذي يمكن يتوليده "
                    "هذا النص هو مثال للنص الذي يمكن يتوليده"
                    " هذا النص هو مثال للنص الذي يمكن يتوليده",
                color: MyColors.white.withOpacity(0.5),
                size: 9),
            Container(
                margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 5)
               , child:
                    MyText(title: "السعر", color: MyColors.primary, size: 13)),
            MyText(
                title:"1000ر.س"
              ,  color: MyColors.white.withOpacity(0.5),
                size: 9),
          ],
        ),
      ),
    );
  }
}
