part of 'SingerDetailsImports.dart';

class SingerDetails extends StatefulWidget {
  @override
  _SingerDetailsState createState() => _SingerDetailsState();
}

class _SingerDetailsState extends State<SingerDetails> {
  @override
  Widget build(BuildContext context) {
    return
      DefaultTabController(length: 3
       , child: Scaffold(
          bottomNavigationBar:
          InkWell( onTap: ()=>AutoRouter.of(context).push(DataBookingRoute())
           , child: Container( height: 40
              , margin: const EdgeInsets.all(10)
              , decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),color: MyColors.primary),

              child: MyText(
                title: "احجز",
                color: MyColors.white,
                size: 13,alien: TextAlign.center,
              ),
            ),
          ),
        backgroundColor: MyColors.secondary, appBar: DefaultAppBar(title: '',)
     , body: Column(
          children: [
            Container( margin: const EdgeInsets.symmetric(horizontal: 5)
              ,child: CachedImage(
                url:
                    'https://downloadwap.com/thumbs2/wallpapers/p2/2019/movies/44/5b6ff85c13556118.jpg',
                height: 230,
                width: MediaQuery.of(context).size.width,
                borderRadius: BorderRadius.circular(5),
              ),
            ),
            Container(margin:  const EdgeInsets.symmetric(vertical: 20)
            ,child: BuildTabBar())
            ,Flexible(
                child:TabBarView(
                children: [
                  Information(),
                  SongsWithMusic(),
                  SongsWithout()
                ]
            ) )
          ],
        ),
    ),
      );
  }
}
