part of 'DetailsWidgetsImports.dart';
class BuildTabBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(child:
    TabBar(
      tabs: [
        Tab(
            child:
            InkWell(
              // onTap: ()=>AutoRouter.of(context).push(CurrentRoute()),
              child:  Container(width: 120,height: 40
                , decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: MyColors.primary),
                child: MyText(title: "المعلومات ", color: MyColors.white, size: 10,alien: TextAlign.center,),
              ),
            )
        ),
        Tab(
            child:
            InkWell(
              //  onTap: ()=>AutoRouter.of(context).push(FinishedRoute()),
              child:  Container(width: 120,height: 40
                , decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: MyColors.primary),
                child: MyText(title: "اغانى بايقاع", color: MyColors.white, size: 10,alien: TextAlign.center,),
              ),
            )

        ),
        Tab(
            child:
            InkWell(
              //  onTap: ()=>AutoRouter.of(context).push(FinishedRoute()),
              child:  Container( height: 40,width: 120
                ,  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: MyColors.primary),
                child: MyText(title: "اغانى بدون ايقاع", color: MyColors.white, size: 10,alien: TextAlign.center,),
              ),
            )

        )
      ],
    ),
    );
  }
}
