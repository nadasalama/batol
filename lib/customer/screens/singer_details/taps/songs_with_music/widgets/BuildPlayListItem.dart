part of 'WidgetsImports.dart';

class BuildPlayListItem extends StatelessWidget {
   final String text1;
   final String text2;
   final String image;

  const BuildPlayListItem({ required this.text1,  required this.text2, required this.image}) ;
  @override
  Widget build(BuildContext context) {
    return
      Container(
      margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 12),
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
      height: 100,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: MyColors.primary.withOpacity(0.5))
          // boxShadow: [BoxShadow(color: MyColors.primary.withOpacity(0.5),)]
          ),
      child: Row(
        children: [
          CachedImage(
            url: image,
            height: 80,
            width: 80,
            borderRadius: BorderRadius.circular(20),
          ),Column(children: [
            MyText(title: text1, color: MyColors.white, size: 12),
            MyText(title: text2, color: MyColors.primary, size: 10)
          ],),Spacer()
          ,Image.asset(Res.playBack,scale: 5,)
        ],
      ),
    );
  }
}
