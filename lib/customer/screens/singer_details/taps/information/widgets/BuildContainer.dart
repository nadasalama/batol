part  of'WidgetsImports.dart';
 class BuildContainer extends StatelessWidget {
    final String text;
    final String title;

  const BuildContainer({ required this.text, required this.title}) ;
   @override
   Widget build(BuildContext context) {
     return
       Container(
         padding:   const EdgeInsets.symmetric(vertical: 10,horizontal: 15),
         margin:   const EdgeInsets.symmetric(vertical: 10,horizontal: 15)
         , child: Column(
         mainAxisAlignment: MainAxisAlignment.start,
         crossAxisAlignment: CrossAxisAlignment.start
         , children: [
         MyText(title: title, color: MyColors.primary, size: 11,
           alien: TextAlign.start,),
         MyText(
           title: text,
           color: MyColors.white,
           size: 10, alien: TextAlign.start,),
         Divider(
           height: 40,color: MyColors.white.withOpacity(0.3),
         ),
       ],
       ),
       );
   }
 }
