part of 'WidgetsMainPageImports.dart';

class BuildAppBar extends PreferredSize {
  final Size preferredSize;
late final  GlobalKey<ScaffoldState> ?scaffold;

  BuildAppBar({required this.preferredSize,this.scaffold})
      : super(child: Container(), preferredSize: preferredSize,);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AppBar(
      backgroundColor: MyColors.secondary,
      leading: Row(
        children: [
          CircleAvatar(
            child: Image.asset(Res.logo),
            radius: 15,
          ),
          Column(
            children: [
              MyText(title: "اهلا وسهلا  ", color: MyColors.primary, size: 11),
              MyText(title: "محمد على ", color: MyColors.primary, size: 10)
            ],
          )
        ],
      ),
      leadingWidth: 100,
      elevation: 0,
      actions: [
        Row(
          children: [
            IconButton(
              icon: Icon(Icons.drag_handle_rounded),
              onPressed: () =>scaffold!.currentState!.openDrawer(),
              color: MyColors.white,
            ),
            IconButton(
              icon: Icon(Icons.notifications),
              onPressed: () =>
                  AutoRouter.of(context).push(NotificationsRoute()),
              color: MyColors.white,
            )
          ],
        ),
      ],
    );
  }
}
