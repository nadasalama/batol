part of 'WidgetsMainPageImports.dart';

class BuildMainList extends StatelessWidget {
  final String image;
  final Function()? onTap;
  final String title;

  const BuildMainList({required this.image, this.onTap,required this.title});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: CachedImage(
          url: image,
          height: 170,
          width: 170,
          borderRadius: BorderRadius.circular(10),
          borderColor: MyColors.primary.withOpacity(0.5),
          alignment: Alignment.bottomCenter,
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 40),
            child: MyText(
              color: MyColors.white,
              size: 12,
              title: title,
              alien: TextAlign.end,
            ),
            padding: const EdgeInsets.symmetric(vertical: 20),
          )),
    );
  }
}
