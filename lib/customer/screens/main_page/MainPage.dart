part of 'MainPageImports.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  MainPageData mainPageData=MainPageData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: mainPageData.scaffold,
      drawer: HomeDrawer(),
        backgroundColor: MyColors.secondary,
        appBar: BuildAppBar(
          preferredSize: Size.fromHeight(80),
          scaffold: mainPageData.scaffold,
        ),
        body: Container(
          margin: const EdgeInsets.symmetric(vertical: 1, horizontal: 5),
          child: Column(
            children: [
              BuildSwiper(image: "https://www.baladnaelyoum.com/images/925x520/383711.jpg",),
              Flexible(
                  child: SingleChildScrollView(
                child: Wrap(
                  verticalDirection: VerticalDirection.down,
                  runSpacing: 10,
                  spacing: 10,
                  children: [
                    BuildMainList(
                      image:
                          "https://mybayutcdn.bayut.com/mybayut/wp-content/uploads/Stage-party-AR11122019-420x230.jpg",
                      onTap: () => AutoRouter.of(context).push(SingersRoute()),
                      title: "المطربين",
                    ),
                    BuildMainList(
                      image:
                          "https://serviis.files.wordpress.com/2017/08/shutterstock_415922566.jpg?w=1000&h=641&crop=1",
                      onTap: () => AutoRouter.of(context).push(SingersRoute()),
                      title: "دى جى ",
                    ),
                    BuildMainList(
                      image:
                          "https://www.kolshe.com/user_images_19_8_2017/3365112.jpg",
                      onTap: () => AutoRouter.of(context).push(ZafatRoute()),
                      title: "زفات",
                    ),
                    BuildMainList(
                      image:
                          "https://www.samma3a.com/tech/ar/wp-content/uploads/sites/3/2017/09/RA_SM_242390-1-1024x544.jpg",
                      onTap: () =>
                          AutoRouter.of(context).push(SingersWomenRoute()),
                      title: "مطربات ",
                    ),
                    BuildMainList(
                      image:
                          "https://mybayutcdn.bayut.com/mybayut/wp-content/uploads/Stage-party-AR11122019-420x230.jpg",
                      onTap: () => AutoRouter.of(context).push(Tansi2IntroRoute()),
                      title: "تنسيق",
                    ),
                    BuildMainList(
                      image:
                          "https://morb3.com/wp-content/uploads/2020/08/13-%D8%B5%D9%88%D8%B1-%D8%B3%D9%8A%D8%A7%D8%B1%D8%A7%D8%AA-%D8%AE%D9%84%D9%81%D9%8A%D8%A7%D8%AA-%D8%B3%D9%8A%D8%A7%D8%B1%D8%A7%D8%AA-%D9%84%D9%84%D9%83%D9%85%D8%A8%D9%8A%D9%88%D8%AA%D8%B1-%D8%B5%D9%88%D8%B1-%D8%AE%D9%84%D9%81%D9%8A%D8%A7%D8%AA-%D8%B3%D9%8A%D8%A7%D8%B1%D8%A7%D8%AA-%D9%81%D8%AE%D9%85%D8%A9-%D8%B3%D8%B7%D8%AD-%D8%A7%D9%84%D9%85%D9%83%D8%AA%D8%A8-"
                              "%D8%AE%D9%84%D9%81%D9%8A%D8%A7%D8%AA-%D8%B3%D9%8A%D8%A7%D8%B1%D8%A7%D8%AA-cars-images-wallpapers-pc-hd-1024x640.jpg",
                      onTap: () => AutoRouter.of(context).push(CarsRoute()),
                      title: " سيارات",
                    ),
                  ],
                ),
              ))
            ],
          ),
        ));
  }
}
