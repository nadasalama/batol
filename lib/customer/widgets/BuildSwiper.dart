part  of'WidgetsImports.dart';
class BuildSwiper extends StatelessWidget {
  final String image ;

  const BuildSwiper({required this.image}) ;
  @override
  Widget build(BuildContext context) {
    return
      Container(
      height: 220,
      child: new Swiper(
        itemCount: 3,
        itemBuilder: (BuildContext context, int index) {
          return new CachedImage(
            url:  image
            ,
            borderRadius: BorderRadius.circular(10),
            haveRadius: true,
            fit: BoxFit.fill,height: 250,width: 100,
          );
        },
        pagination:
        new SwiperPagination(alignment: Alignment.bottomCenter),
        control: new SwiperControl(),
        scrollDirection: Axis.horizontal,
        autoplay: true,
        outer: true,
      ),
    );
  }
}
