part   of 'WidgetsImports.dart';

class BuildContainerBooking extends StatelessWidget {
  final String text;
  final String label;
  const BuildContainerBooking({required this.text, required this.label});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      child: Column( crossAxisAlignment: CrossAxisAlignment.start
        ,children: [
          MyText(title: text, color: MyColors.white, size: 10,alien: TextAlign.start,),
          IconTextFiled(
            label: label,
            validate: (String? value) {},
            controller: TextEditingController(text: ""),
            suffixIcon: Icon(Icons.date_range),
          )
        ],
      ),
    );
  }
}
