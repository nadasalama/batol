
part of'WidgetsImports.dart';
class BuildIntroduction extends StatelessWidget {
    final String image;
    final Function() ?onTap;


  const BuildIntroduction( { required this.image, this.onTap}) ;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: MyText(
            title: "تخطى",
            size: 10,
            color: MyColors.gold,
          ),
          backgroundColor: MyColors.secondary,
          elevation: 0,
        ),
        body:
        Container(
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          width: MediaQuery.of(context).size.width,
          color: MyColors.secondary,
          child: Stack(
            children: [
              Image.asset(
                image,
                fit: BoxFit.fill,
              ),
              Container(
                  margin:
                      const EdgeInsets.symmetric(vertical: 150, horizontal: 25),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MyText(
                            title: "معنا استمتع بالموسيقى ",
                            color: MyColors.primary,
                            size: 13),
                        MyText(
                            title:
                                " هدا النص هو مثال للنص الدي يمكن ان يستبدل ",
                            color: MyColors.white,
                            size: 10),
                        InkWell(
                          onTap: onTap,
                          child:
                          Container(
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: MyColors.primary),
                            height: 60,
                            width: 50,
                            child: Icon(
                              Icons.arrow_back_ios_rounded,
                              size: 25,
                            ),
                          ),
                        )
                      ]))
            ],
          ),
        ));
  }
}
