part  of'WidgetsImports.dart';
class BuildBottomNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(  bottomNavigationBar:
        InkWell( onTap: ()=>AutoRouter.of(context).push(BackToMainRoute())
    , child: Container( height: 40
    , margin: const EdgeInsets.all(10)
    , decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(30),color: MyColors.primary),
    ))
    );}
}
