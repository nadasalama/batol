part of 'WidgetsImports.dart';

class BuildNamesList extends StatelessWidget {
  final String image;
  final String title;
  final Function()? onTap;
  final  String price;

  const BuildNamesList ({required this.image, required this.title, this.onTap, required this.price});
  @override
  Widget build(BuildContext context) {
    return
      Container(
      padding:  const EdgeInsets.symmetric(vertical: 5,horizontal: 5),
      margin: const EdgeInsets.symmetric(vertical: 5,horizontal: 5)
      , child: Column( mainAxisAlignment: MainAxisAlignment.spaceBetween

        ,children: [
          Container( padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 5),
          margin:  const EdgeInsets.symmetric(vertical: 2,horizontal: 5)
          ,  child: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween
             , children: [
                InkWell( onTap: onTap
                   , child:
                    CachedImage(
                url: image,
                height: 100,
                width: 100,
                borderColor: MyColors.primary.withOpacity(0.5),
                borderRadius:   BorderRadius.circular(10),
              ), ),

                InkWell( onTap: onTap
                ,  child: CachedImage(
                    url: image,
                    height: 100,
                    width: 100,
                    borderColor: MyColors.primary.withOpacity(0.5),
                    borderRadius:   BorderRadius.circular(10),
                  ),
                ),
                InkWell( onTap: onTap
                 , child: CachedImage(
                    url: image,
                    height: 100,
                    width: 100,
                    borderColor: MyColors.primary.withOpacity(0.5),
                    borderRadius:   BorderRadius.circular(10),
                  ),
                ),

              ],
            ),
          ),
          Container(  padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 5),
            margin:  const EdgeInsets.symmetric(vertical: 2,horizontal: 2)
           , child: Row( mainAxisAlignment:  MainAxisAlignment.spaceBetween
             , children: [
              MyText(title: title, color: MyColors.white, size: 12),
                MyText(title: title, color: MyColors.white, size: 12),
                MyText(title: title, color: MyColors.white, size: 12)
            ],),
          ),
          Container(  padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 5),
            margin:  const EdgeInsets.symmetric(vertical: 2,horizontal: 5)
            ,child: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween
              ,children: [
                MyText(title: price, color: MyColors.white.withOpacity(0.5), size: 12),
                MyText(title: price, color: MyColors.white.withOpacity(0.5), size: 12),
                MyText(title: price, color: MyColors.white.withOpacity(0.5), size: 12)
              ],),
          )
        ],
      ),
    );
  }
}
